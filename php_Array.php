<?php


    //2 compound data type
        //Associative Array
        // (outer array called= Index array & inner array called= Associative array)

        //for loop
        $person = array(
            "Abul"=>array("Abul Hossain",45,5.7),
            "Halim"=>array("Halim Hossain",49,4.7),
            "Karim"=>array("Karim Uddin",53,5.0),
            "Liakat"=>array("Liakat Ali",30,5.1),
            "Omar"=>array("Omar Sharif Ansary",20,5.6),
        );

        foreach($person as $nick=> $personInfo){
            for($i=0;$i<3;$i++){
                echo "$nick: ".$person[$nick][$i]."<br>";
            }
                echo "<br>";
        }

echo "<br>";
echo "<br>";
echo "<br>";


        //foreach loop
        $person = array(
            "Abul"=>array("Full name"=> "Abul Hossain","Age"=> 45,"Height"=> 5.7),
            "Halim"=>array("Full name"=> "Halim Hossain","Age"=> 49,"Height"=> 4.7),
            "Karim"=>array("Full name"=> "Karim Uddin","Age"=> 53,"Height"=> 5.0),
            "Liakat"=>array("Full name"=> "Liakat Ali","Age"=> 30,"Height"=> 5.1),
            "Omar"=>array("Full name"=> "Omar Sharif Ansary","Age"=> 20,"Height"=> 5.6),
        );

        foreach($person as $nick=> $personInfo){
            foreach($i=0;$i<3;$i++){
                echo "$nick: ".$person[$nick][$i]."<br>";
            }
            echo "<br>";
        }